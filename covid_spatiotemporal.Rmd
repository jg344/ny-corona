---
title: "covid_spatiotemporal"
output:
  pdf_document: default
---
### Load libraries
```{r message = F, warning = F, cache = T}
# install.packages("INLA", repos=c(getOption("repos"),
# INLA="https://inla.r-inla-download.org/R/stable"), dep=TRUE)

library(tidyverse)
library(readr)
library(lubridate)
library(RColorBrewer)
library(spdep)
library(INLA)
library(SpatialEpi)
library(plotly)
library(gganimate)
library(zoo)
```
The readme for this data is available [here](https://github.com/thecityny/covid-19-nyc-data/blob/master/README.md).

### Outcome variables: Positive and Total Tests in NYC by ZCTA
I'll load in the master dataset with updated (more recent) days.
```{r cache = T}
## Each URL has a specific key change to it that can be used to delineate
# each date, Nick made a data frame of these with their corresponding dates
# NOTE: This key can be found by clicking on the clipboard in github history

date1 <- seq(as.Date("2020-05-19"), as.Date("2020-06-08"), by="days")
rm <- c(as.Date('2020-05-20'), as.Date('2020-06-03')) # These are the dates where there is no upload
date1 <- date1[!date1 %in% rm]
keys <- c('a68f42fe3615eaf3d36df219059eb94595cee933',
         'c5cb7417b5ecb836f9727d6996f97d3cd050ba8d',
         '3cbb3b744ed90befb37ea7680fac3aa7f782bb30',
         '8d88b2c06cf6b65676d58b28979731faa10c193c',
         '79d57e636ce99e8525059ed2c8769a04afe39fac',
         'ea689d3d3eb427c4ae9cf1855836b4ceee8f6dab',
         'd52fdfe48dc9b8fe446df048c681b9c19555e469',
         '498a068d6ca1765231b8526a5529ec552a0daff7',
         '65efb1f75b2714bf06fc2e76fba22315727f6943',
         '8636c5557e93e0c5f35b8c2f8ae1ed1e1077d469',
         '3e9a27ce96a59d9107295ade2b2d7255e605b47b',
         '9b5cd4d9259587d41017804dda6589a7b2ab5b3c',
         '62444c1cbfda69c28ed468d14be3f332ea35eea7',
         '53f5d7935feb50fe87132c928f45455af4149407',
         'f094fb20c7843f218cea3bc47a06ebe8055d6127',
         '582977c6b9e14f048305403e1b830145e0669a5e',
         '9e0c1fe569b09ebddbccd6c9f6d519e28920e86a',
         '6328e0b130a4dee1f6140ed3aa8264168d2a925f', 
         '3f21405a00bf7c93db5a5209b44886882feb30a6')
# I can update this if we need to upload more

reference <- data.frame(keys, date1) # This is for referencing codes to their
#respective dates

# Upload data
new_test_data <- data.frame(zcta=NA, positive=NA, total=NA, zcta_cum.perc_pos=NA, timestamp= NA)[numeric(0), ]
for (key in keys) {
  link <- paste('https://raw.githubusercontent.com/nychealth/coronavirus-data/', key,'/tests-by-zcta.csv', sep ='')
  add <- data.frame(zcta=NA, positive=NA, total=NA, zcta_cum.perc_pos=NA, timestamp=NA)[numeric(0), ]
  add <- rbind(add, 
               read_csv(url(link), skip = 2, col_names =FALSE) %>%
                 mutate(date = reference$date1[reference$keys == key])
                 )
  new_test_data <- rbind(new_test_data, add)
}
colnames(new_test_data) = c('zcta', 'positive', 'total', 'zcta_cum.perc_pos', 'timestamp')
new_test_data <- subset(new_test_data,select = -c(zcta_cum.perc_pos))[,c(4,1,2,3)]


### Combine with old data data

old_test_data_url <- 'https://raw.githubusercontent.com/thecityny/covid-19-nyc-data/master/zcta.csv'
old_test_data <- read_csv(url(old_test_data_url))

master_test_data <- rbind(old_test_data, new_test_data)
```

#### Data preparation for spatiotemporal model
First, convert the timestamp to a date alone, removing zcta 99999, and removing any NA zctas. 
```{r}
master_test_data2 <- master_test_data %>%
  mutate(date = ymd(substr(timestamp, start = 1, stop = 10))) %>%
  dplyr::select(date, zcta, positive, total) %>%
  filter(zcta != 99999) %>% # remove 99999
  filter(!is.na(zcta)) # remove the entry with zcta = NA

# how many "zcta"s are there?
length(unique(master_test_data2$zcta))
```
Now, I'd like to verify if these data are taken at the zcta level or at the modzcta level. The readme is a bit vague. To do this, I'll compare the master_test_data2 with the zcta-modzcta crosswalk file. First, I'll see if the zctas in the crosswalk file match up exactly with the zctas in the master_test_data2. 
```{r message = F}
# url for zcta-modzcta crosswalk file
url_zcta_modzcta <- "https://raw.githubusercontent.com/nychealth/coronavirus-data/master/Geography-resources/ZCTA-to-MODZCTA.csv"

zcta_mod_crosswalk <- read_csv(url(url_zcta_modzcta)) %>%
  mutate(zcta = as.character(ZCTA)) %>% # convert to character
  rename(modzcta = MODZCTA) %>%
  dplyr::select(zcta, modzcta)
```
If there are any zctas in the master test data that are not in the "modzctas" of the crosswalk, then the data must have been taken at a zcta level.
```{r}
setdiff(unique(master_test_data2$zcta), unique(zcta_mod_crosswalk$modzcta))
```
There are, so let's convert the test data to modzcta level. To do this, we will need to first left join the crosswalk with the testing data, and then group by modzcta and sum the counts. 
```{r}
# first convert zcta column in master_test_data2 to a character.
master_test_data3 <- master_test_data2 %>%
  mutate(zcta = as.character(zcta))
# left_join with crosswalk file to have modzcta
test_dat <- master_test_data3 %>%
  left_join(zcta_mod_crosswalk, by = "zcta") %>%
  dplyr::select(date, modzcta, zcta, positive, total)
```
The first think I'll do is see if there are any NAs in the positives, since this variable somehow got converted to a character.
```{r}
table(test_dat$positive)[1:5]
```
There are 5 ".". Let's look at these.
```{r}
test_dat %>%
  filter(positive == ".")
```
My first inclination is to delete these. Let's just look at these specific zctas on their dates to get a better sense of what might be going on.
```{r}
test_dat %>%
  filter(zcta == 10111)

test_dat %>%
  filter(zcta == 10165)

test_dat %>%
  filter(zcta == 10170)

test_dat %>%
  filter(zcta == 10171)

test_dat %>%
  filter(zcta == 10278)
```
Each of these entries had zctas that were only reported on a single day. We can delete these. 
```{r}
test_dat2 <- test_dat %>%
  filter(!(zcta %in% c(10111, 10165, 10170, 10171, 10278)))
```
Now, let's see if, for each day, we have the same number of zctas reported.
```{r}
x <- rep(0, length(unique(test_dat2$date)))
for(i in 1:length(unique(test_dat2$date))){
  # grab the date you want to look at
  my_date <- unique(test_dat2$date)[i]
  # subset a data frame to only that date
  dims <- test_dat2 %>%
    filter(date == my_date) %>%
    dim()
  x[i] <- dims[1]
}
print(x)

which(x == 178)
which(x == 184)
```
So, the 8th date has an extra entry and the 49th date has 7 extra entries. Let's check those out.

#### 8th date: 2020-04-10
```{r}
test_dat2 %>%
  filter(date == unique(test_dat2$date)[which(x == 178)]) %>%
  tail()
```
There are two entries for 11679 on 2020-04-10. They're similar. I could just keep one, or add them together. Let's see what the other entries for zcta == 11697 look like.
```{r}
test_dat2 %>%
  filter(zcta == 11697, 
         (date > "2020-04-04" & date < "2020-04-14"))
```
The entry for 2020-04-10 for modzcta=11697 is the same as that for 2020-04-09. We can delete the 

* only keep first entry
```{r}
# remove duplicated entry from dataset
test_dat3 <- test_dat2 %>%
  filter(!(zcta == 11697 & date == "2020-04-10" & positive == 52))

# make sure it worked
test_dat3 %>%
  filter(zcta == 11697 & date > "2020-04-07" & date < "2020-04-12")
```
Perfect! Now let's check the other date with a bunch of extra entries.

#### 49th date: 2020-04-10

```{r}
df <- test_dat3 %>%
  filter(date == unique(test_dat2$date)[which(x == 184)]) %>%
  arrange(date, zcta)

df
```
There must be 7 additional zctas reported here compared to all other days.

For all other days except May 23, are the exact same zctas reported?
```{r}
unique(test_dat3$zcta)
# for each day, make a list of all the zctas. make sure all these are the same, then compare it with the list of zctas from may 23.

# create a subset of all days except May 23. 
sub <- test_dat3 %>%
  filter(date != "2020-05-23")

# initiate empty list
y <- NULL

for(i in 1:length(unique(test_dat3$date))){
  # grab the date you want to look at
  my_date <- unique(test_dat3$date)[i]
  # subset a data frame to only that date
  df <- test_dat3 %>%
    filter(date == my_date)
  y[[i]] <- df$zcta
}

setdiff(as.vector(y[[49]]), as.vector(y[[1]]))
setdiff(as.vector(y[[49]]), as.vector(y[[2]]))

result <- NULL
for(i in c(1:48, 50:length(unique(test_dat3$date)))){
  z <- setdiff(as.vector(y[[49]]), as.vector(y[[i]]))
  if(z[1] == setdiff(as.vector(y[[49]]), as.vector(y[[1]]))[1] & 
     z[2] == setdiff(as.vector(y[[49]]), as.vector(y[[1]]))[2] &
     z[3] == setdiff(as.vector(y[[49]]), as.vector(y[[1]]))[3] &
     z[4] == setdiff(as.vector(y[[49]]), as.vector(y[[1]]))[4] &
     z[5] == setdiff(as.vector(y[[49]]), as.vector(y[[1]]))[5]){
    result[i] <- "yes"
  } else {result[i] <- "no"}
}
# 49th entry will be NA because I didn't ask the for loop to check this comparison. 
result
```
This means that the 7 additional zctas present on May 23 are consistently not present in all other days. So I will delete them. 
```{r}
test_dat4 <- test_dat3 %>%
  filter(!(date == "2020-05-23" & zcta %in% setdiff(as.vector(y[[49]]), as.vector(y[[1]]))))

# check to make sure it worked. 
x2 <- rep(0, length(unique(test_dat4$date)))
for(i in 1:length(unique(test_dat4$date))){
  # grab the date you want to look at
  my_date <- unique(test_dat4$date)[i]
  # subset a data frame to only that date
  dims <- test_dat4 %>%
    filter(date == my_date) %>%
    dim()
  x2[i] <- dims[1]
}
print(x2)
```
Awesome. Now we have consistency across zctas in the dataset.

#### Aggregating from zcta level to modzcta level
Now, need to aggregate the positive and total counts over modzcta, so that there is a single observation for each date and modzcta.
```{r}
test_dat5 <- test_dat4 %>%
  arrange(date, modzcta) %>%
  group_by(date, modzcta) %>%
  dplyr::summarize(positive = sum(as.numeric(positive)),
            total = sum(total))
test_dat5 %>%
  arrange(modzcta, date) 

# write_csv(test_dat5, 
#           path = "output/test_dat_clean_7_15.csv")
```

#### Create a dataset with daily counts (not cumulative)
```{r}
# arrange by modzcta
test_dat6 <- test_dat5 %>%
  arrange(modzcta, date)
test_dat6

# create offset positive and total columns by:
# - moving all observations forward by 1
# - these will be used in later step to be subtracted from the current positives and totals,
# thus resulting in only the new positives and totals for that specific day. 
new_p <- c(0, test_dat6$positive[-length(test_dat6$positive)])
new_t <- c(0, test_dat6$total[-length(test_dat6$total)])

# add in new supplementary variables
test_dat7 <- test_dat6 %>%
  ungroup() %>%
  mutate(shifted_positive = new_p,
         shifted_total = new_t) %>%
  arrange(modzcta, date) %>%
  group_by(modzcta) %>%
  # replace every first row with a 0.
  mutate(shifted_positive = replace(shifted_positive, row_number() == 1, 0), 
         shifted_total = replace(shifted_total, row_number() == 1, 0)) %>%
  mutate(positive_daily = positive - shifted_positive, # subtract to get new daily cases per day
         total_daily = total - shifted_total)
test_dat7

```
Let's see which observations show positive daily counts < 0 . 
```{r}
test_dat7 %>%
  ungroup() %>%
  dplyr::select(date, 
         modzcta, 
         positive_daily, 
         total_daily, 
         shifted_positive, 
         shifted_total, everything()) %>%
  filter(positive_daily < 0)
```

```{r}
# set negative counts to zero for now
test_dat8 <- test_dat7 %>%
  mutate(pos_0 = ifelse(positive_daily < 0, 0, positive_daily),
         tot_0 = ifelse(total_daily < 0, 0, total_daily))
test_dat8
```
#### Calculate 7-day moving average
```{r}
test_dat9 <- test_dat8 %>%
  ungroup() %>%
  arrange(modzcta, date) %>%
  group_by(modzcta) %>%
  mutate(ma_pos_7 = round(rollapply(pos_0,
                                    7,
                                    mean,
                                    align='right',
                                    fill=NA)),
         ma_tot_7 = round(rollapply(tot_0,
                                    7,
                                    mean,
                                    align='right',
                                    fill=NA)))
test_dat9
```
Now, since we are using the 7-day moving average, the new dataset will start on 2020-04-09

Let's subset to data only on and after 2020-04-09.
```{r}
test_dat010 <- test_dat9 %>%
  filter(date > "2020-04-08")
test_dat010
```
Great. There aren't any NAs in the ma_pos_7 or the ma_tot_7 columns, and there aren't any observations where ma_tot_7 equals zero. So we shouldn't have any NAs for calculating expected counts or SIR.

#### Timeseries plots of moving average
```{r}
# create long dataset for plotting
test_dat010_long <- test_dat010 %>%
  dplyr::select(date, modzcta, ma_pos_7, ma_tot_7) %>%
  pivot_longer(cols = ma_pos_7:ma_tot_7,
               names_to = "moving_avg_type",
               values_to = "moving_avg_count")

```

```{r}
# call sample once to permute the list of zctas
perm <- sample(unique(test_dat010[["modzcta"]]), 
               size = length(unique(test_dat010[["modzcta"]])), replace = F)

# assign random samples
rand_z1 <- perm[1:30]
rand_z2 <- perm[31:60]
rand_z3 <- perm[61:90]
rand_z4 <- perm[91:120]
rand_z5 <- perm[121:150]
rand_z6 <- perm[151:177]

# put these in a list
rand_list <- list(rand_z1, rand_z2, rand_z3, rand_z4, rand_z5, rand_z6)
```

#### Timeseries of moving average positive cases by modzcta
```{r}
# moving average for positive counts timeseries
for(i in 1:6){
  # select subset
  my_subset <- rand_list[[i]]
  
  p_time <- test_dat010_long %>%
  filter(modzcta %in% my_subset) %>%
  filter(moving_avg_type == "ma_pos_7") %>%
  mutate(modzcta_fac = as.character(modzcta)) %>%
  ggplot(aes(x = date, y = moving_avg_count, color = modzcta_fac)) +
  scale_y_log10() +
  geom_line() +
  geom_point() + 
  labs(color = "modzcta") +
  ggtitle("Moving average of positive tests by modzcta")
  
  print(p_time)

# ggsave(paste0("output/timeseries_mov_avg_pos.png", i, ".png"),
#        plot = p_time,
#        width = 11,
#        height = 7,
#        units = "in")
}
```

```{r}
# https://journals.plos.org/plosone/article/file?type=supplementary&id=info:doi/10.1371/journal.pone.0194799.s001

# from above website, randomly choose 2 modzctas from each borough. 
random_pick <- as.numeric(c("10458", "10470",
                 "11216", "11225", 
                 "10030", "10009", 
                 "11418", "11418", 
                 "10303", "10312"))

random_pick %in% test_dat010_long$modzcta

p_10 <- test_dat010_long %>%
  filter(modzcta %in% random_pick) %>%
  filter(moving_avg_type == "ma_pos_7") %>%
  mutate(modzcta_fac = as.character(modzcta)) %>%
  ggplot(aes(x = date, y = moving_avg_count, color = modzcta_fac)) +
  scale_y_log10() +
  geom_line() +
  geom_point() + 
  scale_color_manual(values = c(brewer.pal("Dark2", n = 8), 
                                "#99d8c9", "#fdae6b")) +
  labs(color = "modzcta", 
       label = "MODZCTA") +
  ggtitle("Moving Average of Positive Tests in NYC by MODZCTA") +
  theme_minimal()+
  theme(legend.title = element_blank()) +
  xlab("Date") +
  ylab("7-Day Moving Average")

p_10

# ggsave("plots/moving_avg_10_random_modzctas.png",
#        plot = p_10, 
#        width = 6, 
#        height = 5, 
#        units = "in")
```

#### Timeseries of moving averages of total tests by modzcta
```{r}
for(i in 1:6){
  # select subset
  my_subset <- rand_list[[i]]
  
  p_time <- test_dat010_long %>%
  filter(modzcta %in% my_subset) %>%
  filter(moving_avg_type == "ma_tot_7") %>%
  mutate(modzcta_fac = as.character(modzcta)) %>%
  ggplot(aes(x = date, y = moving_avg_count, color = modzcta_fac)) +
  geom_line() +
  scale_y_log10() +
  geom_point() + 
    theme_minimal() +
  ggtitle("Moving average of total tests by modzcta")
  
  print(p_time)

# ggsave(paste0("output/timeseries_mov_avg_tot.png", i, ".png"),
#        plot = p_time,
#        width = 11,
#        height = 7,
#        units = "in")
}

```

#### Calculate expected cases from the moving average totals
To calculate the expected number of cases, we follow two steps:

* calculate a "q" value for each day. q = total number of cases in that day / total number of tests in that day
* To get the expected number for each zcta in this day, multiply the total cases in the zcta by the q for that day. 

```{r}
# for daily counts
test_dat011 <- test_dat010 %>%
  group_by(date) %>%
  dplyr::summarize(sum_positive_ma = sum(ma_pos_7),
            sum_total_ma = sum(ma_tot_7),
            q_ma_7 = sum_positive_ma / sum_total_ma) %>%
  left_join(test_dat010, by = "date") %>%
  mutate(expected_daily_ma = q_ma_7 * ma_tot_7)
test_dat011
```

Next step is to add acs_class and com_class and calculate SIR.
```{r}
# calculate SIR
test_dat_012 <- test_dat011 %>%
  mutate(SIR_ma = ma_pos_7 / expected_daily_ma)
test_dat_012
```

```{r}
# add in acs and com class
# ACS Latent Class
acs_mplus_result_both <- read_csv("output/nyc_ses_2_2_cprobs_plot.csv")[,-1] %>%
  rename(modzcta = MODZCTA,
         acs_class = class) %>%
  dplyr::select(modzcta, acs_class)
acs_mplus_result_both

# 500 Cities latent class
cities_mplus_result <- as.data.frame(read_csv("output/nyc_com_3_cprobs.csv")[,-1]) %>%
  dplyr::select(MODZCTA, COM)%>%
  rename(com_class = COM, 
         modzcta = MODZCTA)
cities_mplus_result
```
Left join the classes.
```{r}
test_dat_013 <- test_dat_012 %>%
  left_join(cities_mplus_result, by = "modzcta") %>%
  left_join(acs_mplus_result_both, by = "modzcta")
test_dat_013
```

#### Calcuate Standardized Incidence Ratios (SIRs)

#### Adding data to the map
First, let's order the dataframe by zcta for converting to wide format.
```{r}
d <- test_dat_013 %>%
  dplyr::select(modzcta, 
         date,
         acs_class,
         com_class,
         ma_pos_7, 
         ma_tot_7,
         expected_daily_ma, 
         SIR_ma) %>%
  arrange(modzcta)
d
```


#### Histograms of SIR
```{r}
d %>%
  ggplot(aes(x = SIR_ma)) +
  geom_histogram(bins = 50)
```

Load the NYC zcta shape object
```{r warning = F}
# updated for modzcta level shapefile
zc_spdf_load <- readRDS("shapefiles/modzcta_spdf_load.rds")
shp_df <- readRDS("shapefiles/modzcta_tidied_shapefile_df.rds")

```

```{r echo = F}
# Rename zcta column to "id"
nyc_dat_join <- d %>%
  mutate(id = as.character(modzcta)) %>%
  dplyr::select(id, date, ma_pos_7, expected_daily_ma, SIR_ma)

# join our data with the mapping shapefile dataframe
nyc_map_dat <- shp_df %>%
  left_join(nyc_dat_join, by = "id")


```

Map SIRs by day (facet_wrap by day)
```{r}
p_sir <- nyc_map_dat %>%
#  filter(SIR_daily < 5) %>%
  ggplot(aes(x=long, y=lat, group=group)) + 
  facet_wrap(~date, dir = "h", ncol = 9) +
  geom_polygon(aes(fill= SIR_ma), show.legend = T) +
  coord_map(projection = "albers", lat0 = 39, lat1 = 45) +
  ggtitle("Moving average SIR") +
  theme_gray() +
  theme(axis.title = element_blank(),
        axis.text.x = element_blank(),
        axis.text.y = element_blank(),
        axis.ticks = element_blank(),
        legend.title = element_blank(),
        panel.background = element_blank()) +
  scale_fill_gradient2(
    midpoint = 1, low = "blue", mid = "white", high = "red"
  )
p_sir

# ggsave("output/sir_daily_all_days.png",
#        plot = p_sir,
#        width = 11,
#        height = 8,
#        units = "in")
```
Let's just map some days from the beginning and end.
```{r}
p_sir_first_last<- nyc_map_dat %>%
  filter(date > "2020-04-08") %>% # eliminate these for equal comparison to moving average maps
  filter(date > "2020-06-01" | date < "2020-04-15") %>%
  ggplot(aes(x=long, y=lat, group=group)) + 
  facet_wrap(~date, dir = "h", ncol = 4) +
  geom_polygon(aes(fill= SIR_ma), show.legend = T) +
  coord_map(projection = "albers", lat0 = 39, lat1 = 45) +
  ggtitle("Daily Standardized Incidence Ratios") +
  theme_gray() +
  theme(axis.title = element_blank(),
        axis.text.x = element_blank(),
        axis.text.y = element_blank(),
        axis.ticks = element_blank(),
        legend.title = element_blank(),
        panel.background = element_blank()) +
  scale_fill_gradient2(
    midpoint = 1, low = "#08589e", mid = "white", high = "#ef3b2c"
  )
p_sir_first_last

# save to folder
# ggsave("output/sir_daily_first_last_ma.png",
#        plot = p_sir_first_last,
#        width = 11,
#        height = 8,
#        units = "in")
```
#### One day from the beginning and one day from the end
```{r}
m1 <- nyc_map_dat %>%
  filter(date == "2020-04-09") %>% # first day
  ggplot(aes(x=long, y=lat, group=group)) + 
  geom_polygon(aes(fill= SIR_ma), show.legend = T) +
  coord_map(projection = "albers", lat0 = 39, lat1 = 45) +
  ggtitle("Standardized Incidence Ratios: 2020-04-09") +
  theme_gray() +
  theme(axis.title = element_blank(),
        axis.text.x = element_blank(),
        axis.text.y = element_blank(),
        axis.ticks = element_blank(),
        legend.title = element_blank(),
        panel.background = element_blank(), 
        legend.key.size = unit(1, "cm"),
        legend.key.width = unit(1,"cm"), 
        legend.text = element_text(size = 12)) +
  scale_fill_gradient2(
    midpoint = 1, low = "#08589e", mid = "white", high = "#ef3b2c"
  )
m1

# save to folder
ggsave("output/sir_day1.png",
       plot = m1,
       width = 11,
       height = 8,
       units = "in")
```

```{r}
m2 <- nyc_map_dat %>%
  filter(date == "2020-06-08") %>% # last day
  ggplot(aes(x=long, y=lat, group=group)) + 
  geom_polygon(aes(fill= SIR_ma), show.legend = T) +
  coord_map(projection = "albers", lat0 = 39, lat1 = 45) +
  ggtitle("Standardized Incidence Ratios: 2020-06-08") +
  theme_gray() +
  theme(axis.title = element_blank(),
        axis.text.x = element_blank(),
        axis.text.y = element_blank(),
        axis.ticks = element_blank(),
        legend.title = element_blank(),
        panel.background = element_blank(),
        legend.key.size = unit(1, "cm"),
        legend.key.width = unit(1,"cm"), 
        legend.text = element_text(size = 12)) +
  scale_fill_gradient2(
    midpoint = 1, low = "#08589e", mid = "white", high = "#ef3b2c"
  )
m2

# save to folder
ggsave("output/sir_day_last.png",
       plot = m2,
       width = 11,
       height = 8,
       units = "in")
```



#### Time series plots
```{r}
g <- d %>%
  ggplot(aes(x = date, 
             y = SIR_ma, 
             group = modzcta, 
             color = factor(modzcta))) +
  geom_line() +
  geom_point(size = 2) + 
  theme_bw() + 
  theme(legend.position = "none") +
  ggtitle("Moving average SIRs Over Time by MODZCTA in NYC")
g
```
```{r}
# ggplotly(g)
```

#### Modeling
Convert shapefile to neighborhood object with poly2nb() function. Below shows first six neighborhood adjacencies. 
```{r}
# how do we know if this is doing the modzcta attribute???
nb <- poly2nb(zc_spdf_load)
head(nb)

length(nb)
```
Now, we convert the neighborhood object to an INLA object for modeling. Then, we read the graph into INLA with the inla.read.graph() function. 
```{r}
nb2INLA("shapefiles/nyc_map_modzcta.adj", nb)
gr <- inla.read.graph(filename = "shapefiles/nyc_map_modzcta.adj")
```

Next, we create index vectors for counties and years that will be used to specify the random effects of the model:

* idarea is the vector with the indices of counties and has elements 1 to 177 (number of modzctas)

* idtime is the vector with the indices of dates and has elements 1 to 30 (number of days)

We create a second index vector for modzctas since each vector can only be associated with an f() function once in INLA. 
```{r}
# d <- test_dat_013
# create unique zctas, zcta counter, zcta counter
z <- unique(d$modzcta)
idarea <- 1:length(z)
idarea1 <- 1:length(z)

z_area <- data.frame(modzcta = z, idarea = idarea, idarea1 = idarea1)

# create unique dates, date counter
da <- unique(d$date)
idtime <- 1:length(da)

z_time <- data.frame(date = da, idtime = idtime)

# merge everything: now has counters for area and time. 
d2 <- d %>%
  left_join(z_area, by = "modzcta") %>%
  left_join(z_time, by = "date")
d2

```
Bring in covariates: 

* proportion over 65 years old
* proportion male
* proportion non-hispanic black
* proportion hispanic
* management and professional
```{r}
covariates <- readRDS("covariates.rds")
covariates <- covariates %>%
  mutate(modzcta = as.double(modzcta)) # convert modzcta to a double to match d2 object

# join d2 object with covariates data frame
d3 <- d2 %>%
  left_join(covariates, by = "modzcta")
d3

# save to rds for running models
saveRDS(d3, file = "modeling_dataset.rds")
```


























## Fit models in INLA

### model: july 22- similar to Coker
```{r}
m_coker <- ma_pos_7 ~ male_prop + 
  pop_non_hispanic_black_prop +
  hispanic_prop +
  age_over_65_prop +
  male_prop*pop_non_hispanic_black_prop +
  idtime +
  f(com_class, model = "iid") +
  f(acs_class, model = "iid") + 
  f(idarea, model = "bym", graph = gr) +
  f(idtime, model = "ar1")

res_coker <- inla(m_coker,
                 family = "poisson",
                 data = d3, 
                 E = expected_daily_ma,
                 control.predictor = list(compute = TRUE), 
                 control.compute = list(dic = TRUE))

probs_com <- as.numeric(lapply(res_coker$marginals.random$com_class, function(X){ 1 - inla.pmarginal(0, X) }))

probs_acs <- as.numeric(lapply(res_coker$marginals.random$acs_class, function(X){ 1 - inla.pmarginal(0, X) }))

probs_com
probs_acs

res_coker$summary.random

save(res_coker,file="res_coker.bym.obj")
summary(res_coker)
```


### m00: only fixed effects
```{r}

m00 <- ma_pos_7 ~ male_prop + 
  pop_non_hispanic_black_prop +
  hispanic_prop +
  age_over_65_prop

res_m00 <- inla(m00,
                 family = "poisson",
                 data = d3, 
                 E = expected_daily_ma,
                 control.predictor = list(compute = TRUE), 
                 control.compute = list(dic = TRUE))
```

```{r}
summary(res_m00)
res_m00$summary.fixed
res_m00$dic$dic
```

### m0: Null model with only fixed effects, classes as fixed effects
```{r}
# m0: Null model with only fixed effects
m0 <- ma_pos_7 ~ male_prop + 
  pop_non_hispanic_black_prop +
  hispanic_prop +
  age_over_65_prop +
  as.factor(com_class) +
  as.factor(acs_class)

res_m0 <- inla(m0,
                 family = "poisson",
                 data = d3, 
                 E = expected_daily_ma,
                 control.predictor = list(compute = TRUE), 
                 control.compute = list(dic = TRUE))
```
Overall summary.
```{r}
summary(res_m0)
```
Summary of fixed effects
```{r}
res_m0$summary.fixed
```
DIC.
```{r}
res_m0$dic$dic
```

### m01: fixed effects, classes as fixed, plus spatial and time components
```{r}
m01 <- ma_pos_7 ~ male_prop + 
  pop_non_hispanic_black_prop +
  hispanic_prop +
  age_over_65_prop +
  as.factor(com_class) +
  as.factor(acs_class) + 
  f(idarea, model = "bym", graph = gr) +
  f(idtime, model = "ar1")

res_m01 <- inla(m0,
                 family = "poisson",
                 data = d3, 
                 E = expected_daily_ma,
                 control.predictor = list(compute = TRUE), 
                 control.compute = list(dic = TRUE))
```

```{r}
summary(res_m01)
res_m01$summary.fixed
res_m01$summary.random
res_m01$dic$dic
```

### m1: fixed effects with classes as random effects, spatial and time random effects
```{r}
m1 <- ma_pos_7 ~ male_prop + 
  pop_non_hispanic_black_prop +
  hispanic_prop +
  age_over_65_prop +
  f(com_class, model = "iid") +
  f(acs_class, model = "iid") + 
  f(idarea, model = "bym", graph = gr) +
  f(idtime, model = "ar1")

res_m1<- inla(m1,
                 family = "poisson",
                 data = d3, 
                 E = expected_daily_ma,
                 control.predictor = list(compute = TRUE), 
                 control.compute = list(dic = TRUE))
```

```{r}
summary(res_m1)
```

```{r}
res_m1$summary.random
```

### m2: fixed effects with classes BOTH as random effects and fixed effects, spatial and time
```{r}
m2 <- ma_pos_7 ~ male_prop + 
  pop_non_hispanic_black_prop +
  hispanic_prop +
  age_over_65_prop +
  as.factor(com_class) +
  as.factor(acs_class) +
  f(com_class, model = "iid") +
  f(acs_class, model = "iid") + 
  f(idarea, model = "bym", graph = gr) +
  f(idtime, model = "ar1")

res_m2<- inla(m2,
                 family = "poisson",
                 data = d3, 
                 E = expected_daily_ma,
                 control.predictor = list(compute = TRUE), 
                 control.compute = list(dic = TRUE))
```

```{r}
summary(res_m2)
res_m2$summary.fixed
res_m2$summary.random
res_m2$dic$dic
```

### m3: fixed effects with classes BOTH as random effects and fixed effects, plus interactions
```{r}
m3 <- ma_pos_7 ~ male_prop + 
  pop_non_hispanic_black_prop +
  hispanic_prop +
  age_over_65_prop +
  male_prop*pop_non_hispanic_black_prop +
  male_prop*pop_non_hispanic_black_prop +
  as.factor(com_class) +
  as.factor(acs_class) +
  f(com_class, model = "iid") +
  f(acs_class, model = "iid") + 
  f(idarea, model = "bym", graph = gr) +
  f(idtime, model = "ar1")

res_m3<- inla(m3,
                 family = "poisson",
                 data = d3, 
                 E = expected_daily_ma,
                 control.predictor = list(compute = TRUE), 
                 control.compute = list(dic = TRUE))
```

```{r}
summary(res_m3)
res_m3$summary.fixed
res_m3$summary.random
res_m3$dic$dic
```

### m4: fixed effects with classes only as fixed effects, plus interactions
```{r}
m4 <- ma_pos_7 ~ male_prop + 
  pop_non_hispanic_black_prop +
  hispanic_prop +
  age_over_65_prop +
  male_prop*pop_non_hispanic_black_prop +
  male_prop*pop_non_hispanic_black_prop +
  as.factor(com_class) +
  as.factor(acs_class) +
  f(idarea, model = "bym", graph = gr) +
  f(idtime, model = "ar1")

res_m4<- inla(m4,
                 family = "poisson",
                 data = d3, 
                 E = expected_daily_ma,
                 control.predictor = list(compute = TRUE), 
                 control.compute = list(dic = TRUE))
```

```{r}
summary(res_m4)
res_m4$summary.fixed
res_m4$summary.random
res_m4$dic$dic
```

### m5: fixed effects with classes only as random effects, plus interactions
```{r}
m5 <- ma_pos_7 ~ male_prop + 
  pop_non_hispanic_black_prop +
  hispanic_prop +
  age_over_65_prop +
  male_prop*pop_non_hispanic_black_prop +
  male_prop*pop_non_hispanic_black_prop +
  f(com_class, model = "iid") +
  f(acs_class, model = "iid") +
  f(idarea, model = "bym", graph = gr) +
  f(idtime, model = "ar1")

res_m5<- inla(m5,
                 family = "poisson",
                 data = d3, 
                 E = expected_daily_ma,
                 control.predictor = list(compute = TRUE), 
                 control.compute = list(dic = TRUE))
```

```{r}
summary(res_m5)
res_m5$summary.fixed
res_m5$summary.random
res_m5$dic$dic
```






Without covariates.
```{r}
formula_ma_7_15 <- ma_pos_7 ~ f(idarea, model = "bym", graph = gr) +
  f(idarea1, idtime, model = "iid") + idtime +
  f(com_class, model = "iid") +
  f(acs_class, model = "iid")

res_ma_7_15 <- inla(formula_ma_7_15,
            family = "poisson", 
            data = d2, 
            E = expected_daily_ma,
            control.predictor = list(compute = TRUE))
```

```{r}
# adding covariates
formula_covs <- ma_pos_7 ~ male_prop + 
  pop_non_hispanic_black_prop +
  hispanic_prop +
  age_over_65_prop +
  f(idarea, model = "bym", graph = gr) +
  f(idarea1, idtime, model = "iid") + idtime +
  f(com_class, model = "iid") +
  f(acs_class, model = "iid")

res_covs <- inla(formula_covs,
                 family = "poisson",
                 data = d3, 
                 E = expected_daily_ma,
                 control.predictor = list(compute = TRUE), 
                 control.compute = list(dic = TRUE))
```

#### Summary of Model
```{r}
summary(res_covs)
```

```{r}
res_covs$summary.fixed
```


```{r}
res_covs$summary.random
```
```{r}
hist(res_covs$summary.random[[1]]$mean)
hist(res_covs$summary.random[[2]]$mean)
```

```{r}
# model with time as a random effect with AR1
formula_covs_ar1 <- ma_pos_7 ~ male_prop + 
  pop_non_hispanic_black_prop +
  hispanic_prop +
  age_over_65_prop +
  f(idarea, model = "bym", graph = gr) +
  f(idtime, model = "ar1") +
  f(com_class, model = "iid") +
  f(acs_class, model = "iid")

res_covs_ar1 <- inla(formula_covs_ar1,
                 family = "poisson",
                 data = d3, 
                 E = expected_daily_ma,
                 control.predictor = list(compute = TRUE))
```

```{r}
summary(res_covs_ar1)
```

```{r}
res_covs_ar1$summary.fixed
```


```{r}
res_covs_ar1$summary.random
```

#### Remaining questions:

* How will time be specified? What kind of correlation structure?

* Do we want to model a space-time interaction?

* Specifying priors for the model


```{r}
names(inla.models()$latent)
```
```{r}
inla.doc("bym")
```

```{r}
# lower is better!
# res$dic$dic
```

Summaries for random effect of modzcta. 
```{r}
# res$summary.random
```
#### Mapping relative risks
Add fitted values to d2.
```{r eval = F}
# dim(d2)
# d2$RR <- res$summary.fitted.values[, "mean"]
# d2$LL <- res$summary.fitted.values[, "0.025quant"]
# d2$UL <- res$summary.fitted.values[, "0.975quant"]
# d2
```
Join this dataframe with shapefile for NYC
```{r eval = F}
# Rename zcta column to "id"
# n_dat_join <- d2 %>%
#   mutate(id = as.character(zcta)) %>%
#   select(id, date, pos_0, expected_daily, SIR, RR, LL, UL)
# 
# # join our data with the mapping shapefile dataframe
# map_dat <- shp_df %>%
#   left_join(n_dat_join, by = "id")
```

##### RR
```{r eval = F}
p_RR <- map_dat %>%
  filter(!is.na(date)) %>%
  filter(date > "2020-05-09" | date < "2020-04-07") %>%
  ggplot(aes(x=long, y=lat, group=group)) + 
  facet_wrap(~date, dir = "h", ncol = 4) +
  geom_polygon(aes(fill= RR), show.legend = T) +
  coord_map(projection = "albers", lat0 = 39, lat1 = 45) +
  ggtitle("Relative Risk") +
  theme_gray() +
  theme(axis.title = element_blank(),
        axis.text.x = element_blank(),
        axis.text.y = element_blank(),
        axis.ticks = element_blank(),
        legend.title = element_blank(),
        panel.background = element_blank()) +
  scale_fill_gradient2(
    midpoint = 1, low = "blue", mid = "white", high = "red"
  )
p_RR

ggsave("output/RR_last_12.png",
       plot = p_RR,
       width = 11,
       height = 8,
       units = "in")
```

