rm(list = ls())
library(lme4)
library(glmmTMB)
library(dplyr)
library(car)
library(multcomp)
options(scipen = 999)
# setwd("C:/Users/jg344/Desktop/covid project/new/")

dir <- "C:/Users/jg344/Desktop/covid project/out2/daily.p2"
# dir <- "C:/Users/jg344/Desktop/lauren"
setwd(dir)
list.files(dir)

# "case.P1.f.m1.rds" "case.P1.f.m2.rds" 
# "case.P1.r.m1.rds" "case.P1.r.m2.rds" "case.P1.r.m3.rds" "case.P1.r.m4.rds"
   # "case.P2.f.m1.rds"  "case.P2.f.m2.rds" 
   # "case.P2.r.m1.rds"  "case.P2.r.m2.rds"  "case.P2.r.m3.rds"  "case.P2.r.m4.rds"  
   # "death.P2.f.m1.rds" "death.P2.f.m2.rds" 
   # "death.P2.r.m1.rds" "death.P2.r.m2.rds" "death.P2.r.m3.rds"  "death.P2.r.m4.rds"  

case.P1.f.m5 <- readRDS("case.P1.f.m8.rds")
                "case.P1.f.m8.rds"
                "case.P2.f.m6.rds"
                
                "case.P2.f.m5.rds"
                "case.P2.f.m6.rds"

                
m.result <- readRDS("case.P2.f.m6.rds")
summary(m.result)
aictab <- summary(m.result)$AICtab %>% as.data.frame()
write.csv(aictab, "aic.csv")

g1 <- glht(m.result, linfct = mcp(acs = "Tukey"))
sg1 <- summary(g1, test = adjusted("none"))
dt1 <- as.data.frame(cbind(sg1$test$coefficients, sg1$test$sigma, sg1$test$tstat, sg1$test$pvalues))
names(dt1) <- c("Estimate", "Std.Error", "Z value",  "Pr(>|z|)")
dt2 <- dt1 %>% cbind(linear.contrast = rownames(dt1)) %>%  mutate(estimate = round(Estimate, 4),
               std.error = round(Std.Error, 4),
               z.value = round(`Z value`,3),
               p.value = ifelse(`Pr(>|z|)` < 0.001, "< 0.001", as.character(round(`Pr(>|z|)`,4))))
write.csv(dt2, "dt2.csv")



coeff <- summary(m.result)$coefficients$cond

outab <-  coeff %>% as.data.frame %>% cbind(coefficient = rownames(coeff)) %>%
               mutate(estimate = round(Estimate, 4),
                      std.error = round(`Std. Error`, 4),
                      z.value = round(`z value`,3),
                      p.value = ifelse(`Pr(>|z|)` < 0.001, "< 0.001", as.character(round(`Pr(>|z|)`,4))))%>%
          dplyr::select(coefficient, estimate, std.error, z.value, p.value)

outab

write.csv(outab, "m.result.csv")

anovtab <- car::Anova(m.result)

anovatab <- anovtab %>% as.data.frame %>% cbind(effect = rownames(anovtab)) %>%
  mutate(Chisq_ = round(Chisq, 4),
         p.value = ifelse(`Pr(>Chisq)` < 0.001, "< 0.001", as.character(round(`Pr(>Chisq)`,4)))) %>%
  dplyr::select(effect, Chisq_, Df,  p.value)

anovatab

write.csv(anovatab, "ano.result.csv")

                                                                   
                                                                   
