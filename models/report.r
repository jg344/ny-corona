rm(list = ls())

library(lme4)
library(glmmTMB)
library(dplyr)
options(scipen = 999)
setwd("C:/Users/jg344/Desktop/covid project/new/")

m.result <- readRDS("weekly.p2/death.W.p2.com.2.rds")

coeff <- summary(m.result)$coefficients$cond



outab <-  coeff %>% as.data.frame %>% cbind(coefficient = rownames(coeff)) %>% 
               mutate(estimate = round(Estimate, 2),
                      std.error = round(`Std. Error`, 3),
                      z.value = round(`z value`,3),
                      p.value = ifelse(`Pr(>|z|)` < 0.001, "< 0.001", as.character(round(`Pr(>|z|)`,4)))) %>% 
          select(coefficient, estimate, std.error, z.value, p.value)

write.csv(outab, "weekly.p2/m.result.csv")


