rm(list = ls())
options(scipen = 999)
load("C:/Users/jg344/Desktop/covid project/new/out/data.RData")

# p1.cum <- daily.p1 %>% group_by(modzcta) %>%
#   mutate(cum.case = cumsum(case_daily)) %>%
#   slice(tail(row_number(), 1))

# library(MASS)
# library(AER)
# deviance(fix.cum.1)/fix.cum.1$df.residual
# dispersiontest(fix.cum.3)
# 
# fix.cum.1 <- glm.nb(cum.case ~ acs + com + 
#          scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
#          offset(log(pop_denom)) , data = p1.cum)
# summary(fix.cum.1) 
# rootogram(fix.cum.1)
# influencePlot(fix.cum.1)
# AIC(fix.cum.1, fix.cum.2)
# 
# summary(fix.cum.1)
# anova(fix.cum.1, theta =  17.48)
# fix.cum.3 <- glm(cum.case ~ acs + com + 
#                    scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
#                    offset(log(pop_denom)) , data = p1.cum, family = "poisson")
# pchisq(2 * (logLik(fix.cum.1) - logLik(fix.cum.3)), df = 1, lower.tail = FALSE)
# 
# fix.cum.2 <- glm.nb(cum.case ~ acs + com +
#               scale(age_over_65_prop) + scale(hispanic_prop) +
#               offset(log(pop_denom)) , data = p1.cum)
# anova(fix.cum.2, fix.cum.1)
# 
# summary(fix.cum, dispersion = 17.4836)
# anova(fix.cum)
# g1 <- glht(fix.cum, linfct = mcp(acs = "Tukey"))


# -------------------------------------------------------------------------


library(dplyr)
library(tidyr)
library(plyr)
library(ggplot2)

setwd("C:/Users/jg344/Desktop/covid project/out2/daily.p2")
m.nb <- readRDS("case.P2.f.m2.rds")
m.zi <- readRDS("case.P2.f.m5.rds")
m.gau <- readRDS("case.P2.f.m4.rds")
  
summary(m2)
daily.p2$predict  <- predict(object =m.gau, newdata =daily.p2)
summary(daily.p2$predict); summary(daily.p2$case_daily)

see <- daily.p2 %>% mutate(predict.r = ifelse(predict< 0, 0,round(predict,1))) %>% 
  dplyr::select(case_daily, predict.r) 

t1 <- plyr::count(see,"case_daily") %>% dplyr::rename(count = case_daily) %>% dplyr::mutate(group = "case daily")
# t2 <- plyr::count(see, "case.7dayavg") %>% dplyr::rename(count = case.7dayavg) %>% dplyr::mutate(group = "case 7dayavg")
t2 <- plyr::count(see, "predict.r") %>% dplyr::rename(count = predict.r) %>% dplyr::mutate(group = "case predicted")
rooto.t <- rbind(t1,t2,t3)
# p <- ggplot(rooto.t, aes(x = count, y = sqrt(freq), group = group))
# p + geom_line(aes(linetype = group, color = group))

p <- ggplot(t1, aes(count, sqrt(freq)))
p <- p + geom_bar(stat="identity", color = "darkblue", fill = "white", size = 1) 
p1 <- p + geom_line(data=t2, aes(x=count, y=sqrt(freq)), colour="red", size = 1) +
  xlab("case count") + ylab("sqrt (Frequency)") 

png('m.gau.png', units = 'in', width = 5, height = 4, res = 300)
p1
dev.off()






