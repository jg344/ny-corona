rm(list = ls())

library(glmmTMB)
options(scipen = 999)
load("C:/Users/jg344/Desktop/covid project/new/out/analysis data.RData")
# setwd("C:/Users/jg344/Desktop/covid project/new/out2/")

# Daily.p1 -------------------------------------------------------------------------

# response variables: case_daily, case.7dayavg
# primary covariates of interest: acs, com
# covariates: male_prop ,age_over_65_prop, pop_non_hispanic_black_prop, hispanic_prop
# offset: pop_denom

# negative binomial model: 
setwd("C:/Users/jg344/Desktop/covid project/out2/daily.p1")

case.P1.r.m1 <- glmmTMB(case_daily ~ acs + com +
                            scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                            offset(log(pop_denom)) + (1|modzcta), data = daily.p1, 
                          family = nbinom2)

# summary(case.P1.r.m1)
saveRDS(case.P1.r.m1, "case.P1.r.m1.rds")


# case.P1.r.m1.avg <- glmmTMB( case.7dayavg ~ acs + com + 
                                # scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                                # offset(log(pop_denom)) + (1|modzcta), data = daily.p1,
                              # family = nbinom2)
# summary(case.P1.r.m1.avg)
# saveRDS(case.P1.r.m1.avg, "case.P1.r.m1.avg.rds")


case.P1.r.m2 <- glmmTMB(case_daily ~ acs + com + seq.d +
                  scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                  offset(log(pop_denom)) + (1|modzcta), data = daily.p1,
                  family = nbinom2)
# summary(case.P1.r.m2)
saveRDS(case.P1.r.m2, "case.P1.r.m2.rds")


case.P1.r.m3 <- glmmTMB(case_daily ~ acs + com + seq.d + acs*seq.d + 
                  scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                  offset(log(pop_denom)) + (1|modzcta), data = daily.p1,
                family = nbinom2)
summary(case.P1.r.m3)
saveRDS(case.P1.r.m3, "case.P1.r.m3.rds")

case.P1.r.m4 <- glmmTMB(case_daily ~ acs + com + seq.d + com*seq.d + 
                scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                offset(log(pop_denom)) + (1|modzcta), data = daily.p1,
              family = nbinom2)
# summary(case.P1.r.m4)
saveRDS(case.P1.r.m4, "case.P1.r.m4.rds")


case.P1.f.m1 <- glmmTMB(case_daily ~ acs + com + seq.d + acs*seq.d + com*seq.d +
                scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                offset(log(pop_denom)) + (1|modzcta), data = daily.p1,
              family = nbinom2)
saveRDS(case.P1.f.m1, "case.P1.f.m1.rds")

# summary(case.P1.f.m1)

# time in the random effect 
# daily.p1$modzcta_1 <- factor(daily.p1$modzcta)
# daily.p1$seq.d_1 <- factor(daily.p1$seq.d)
# levels(daily.p1$seq.d_1)

# Warning message:
#   In fitTMB(TMBStruc) :
#   Model convergence problem; false convergence (8). See vignette('troubleshooting')

case.P1.f.m2 <- glmmTMB(case_daily ~ acs + com + seq.d + acs*seq.d + com*seq.d +
                          scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                          offset(log(pop_denom)) + (seq.d |modzcta) , data = daily.p1,
                        family = nbinom2)

case.P1.f.m7 <- glmmTMB(case_daily ~ acs + com + seq.d + acs*seq.d + com*seq.d +
                          scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                          offset(log(pop_denom)) + (seq.d |modzcta) , data = daily.p1,
                        family = poisson)
check_overdispersion( case.P1.f.m7)
com.tab <- anova(case.P1.f.m7, case.P1.f.m2)
# ranef(case.P1.f.m2)
# summary(case.P1.f.m2)
saveRDS(case.P1.f.m2, "case.P1.f.m2.rds")

case.P1.f.m3 <- glmmTMB(case_daily ~ acs + com + seq.d + acs*seq.d + com*seq.d +
                          scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                          offset(log(pop_denom)) + (1|modzcta), data = daily.p1,
                        family = nbinom2, ziformula  = ~ 1)
saveRDS(case.P1.f.m3, "case.P1.f.m3.rds")


case.P1.f.m4 <-  glmmTMB(case_daily ~ acs + com + seq.d + acs*seq.d + com*seq.d +
                           scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                           offset(log(pop_denom)) + (1|modzcta), data = daily.p1,
                         family = poisson, ziformula  = ~ 1)

saveRDS(case.P1.f.m4, "case.P1.f.m4.rds")

case.P1.f.m5 <- glmmTMB(case_daily ~ acs + com + seq.d + acs*seq.d + com*seq.d +
                          scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                          offset(log(pop_denom)) + (seq.d|modzcta), data = daily.p1,
                        family = nbinom2, ziformula  = ~ 1)
saveRDS(case.P1.f.m5, "case.P1.f.m5.rds")

case.P1.f.m6 <- glmmTMB(case_daily ~ acs + com + seq.d + acs*seq.d + com*seq.d +
                          scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                          offset(log(pop_denom)) + (seq.d|modzcta), data = daily.p1,
                        family = nbinom2, ziformula  = ~ acs+com)
saveRDS(case.P1.f.m6, "case.P1.f.m6.rds")

setwd("C:/Users/jg344/Desktop/covid project/out2/daily.p1")
case.P1.f.m1 <- readRDS("case.P1.f.m1.rds")
case.P1.f.m2 <- readRDS("case.P1.f.m2.rds")
case.P1.r.m1 <- readRDS("case.P1.r.m1.rds")
case.P1.r.m2 <- readRDS("case.P1.r.m2.rds")
case.P1.r.m3 <- readRDS("case.P1.r.m3.rds")
case.P1.r.m4 <- readRDS("case.P1.r.m4.rds")
anova(case.P1.r.m3, case.P1.r.m4)
com.tab <- anova(case.P1.r.m1, case.P1.r.m2, case.P1.r.m3, case.P1.r.m4, case.P1.f.m1, case.P1.f.m2)
write.csv(com.tab, "comtab1.csv")
anova(case.P1.f.m2, case.P1.f.m5)

com.tab <- anova(case.P1.f.m2, case.P1.f.m5, case.P1.f.m6)

case.P1.f.m5 <- readRDS("case.P1.f.m5.rds")

case.P1.f.m8 <- glmmTMB(case_daily ~ acs + com + borough + seq.d + acs*seq.d + com*seq.d +
                          scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                          offset(log(pop_denom)) + (seq.d|modzcta), data = daily.p1,
                        family = nbinom2, ziformula  = ~ 1)
saveRDS(case.P1.f.m8, "case.P1.f.m8.rds")
summary(case.P1.f.m8)



# case.P1.f.m2 <- glmmTMB(case_daily ~ acs + com + seq.d + acs*seq.d + com*seq.d +
#               scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
#                 offset(log(pop_denom)) + (1|modzcta) + ar1(seq.d| modzcta), data = daily.p1,
#               family = nbinom2)

# saveRDS(case.D.p1.acs.avg, "daily.p1/case.D.p1.acs.avg.rds")

# Daily.p2 -------------------------------------------------------------------------

# response variable: case_daily, case.7dayavg, 
#                    death_daily, death.7dayavg
# primary covariates of interest: acs, com
# covariates: male_prop ,age_over_65_prop, pop_non_hispanic_black_prop, hispanic_prop
# offset: pop_denom
setwd("C:/Users/jg344/Desktop/covid project/out2/daily.p2")

# case

case.P2.r.m1 <- glmmTMB(case_daily ~ acs + com +
                            scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                            offset(log(pop_denom)) + (1|modzcta), data = daily.p2,
                          family = nbinom2)
# summary(case.P2.r.m1)
saveRDS(case.P2.r.m1, "case.P2.r.m1.rds")

case.P2.r.m2 <- glmmTMB( case_daily ~ acs + com + seq.d +
                            scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                            offset(log(pop_denom)) + (1|modzcta), data = daily.p2,
                          family = nbinom2)
# summary(case.P2.r.m2)
saveRDS(case.P2.r.m2, "case.P2.r.m2.rds")


case.P2.r.m3 <- glmmTMB( case_daily ~ acs + com + seq.d + acs*seq.d +
                                scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                                offset(log(pop_denom)) + (1|modzcta), data = daily.p2,
                              family = nbinom2)
# summary(case.P2.r.m3)
saveRDS(case.P2.r.m3, "case.P2.r.m3.rds")

case.P2.r.m4 <- glmmTMB( case_daily ~ acs + com + seq.d + com*seq.d +
                                scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                                offset(log(pop_denom)) + (1|modzcta), data = daily.p2,
                              family = nbinom2)
# summary(case.P2.r.m4)
saveRDS(case.P2.r.m4, "case.P2.r.m4.rds")


case.P2.f.m1 <- glmmTMB(case_daily ~ acs + com + seq.d + acs*seq.d + com*seq.d +
                          scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop)+ scale(hispanic_prop) +
                          offset(log(pop_denom)) + (1|modzcta), data = daily.p2, 
                        family = nbinom2)
saveRDS(case.P2.f.m1, "case.P2.f.m1.rds")


# daily.p2$modzcta_1 <- factor(daily.p2$modzcta)
# daily.p2$seq.d_1 <- factor(daily.p2$seq.d)
# levels(daily.p2$seq.d_1)

# case.P2.f.m2 <- glmmTMB(case_daily ~ acs + com + seq.d +  acs*seq.d + com*seq.d +
                          # scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop)+
                          # offset(log(pop_denom)) + (1|modzcta) + ar1(seq.d_1 + 0| modzcta_1), data = daily.p2)

case.P2.f.m2 <- glmmTMB(case_daily ~ acs + com + seq.d  + acs*seq.d + com*seq.d +
                          scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                          offset(log(pop_denom)) + (seq.d|modzcta) , data = daily.p2, 
                        family = nbinom2)
saveRDS(case.P2.f.m2, "case.P2.f.m2.rds")

case.P2.r.m1 <- readRDS("case.P2.r.m1.rds")
case.P2.r.m2 <- readRDS("case.P2.r.m2.rds")
case.P2.r.m3 <- readRDS("case.P2.r.m3.rds")
case.P2.r.m4 <- readRDS("case.P2.r.m4.rds")
case.P2.f.m1 <- readRDS("case.P2.f.m1.rds")
case.P2.f.m2 <- readRDS("case.P2.f.m2.rds")
com.tab <- anova(case.P2.r.m1, case.P2.r.m2, case.P2.r.m3, case.P2.r.m4, case.P2.f.m1, case.P2.f.m2)
write.csv(com.tab, "com.tab.csv")

case.P2.f.m3 <- glmmTMB(case_daily ~ acs + com + seq.d  + acs*seq.d + com*seq.d + 
                          scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                          offset(log(pop_denom)) + (seq.d|modzcta) , data = daily.p2,
                        family = poisson)
saveRDS(case.P2.f.m3, "case.P2.f.m3.rds")

com.tab <- anova(case.P2.f.m3, case.P2.f.m2)
check_overdispersion(case.P2.f.m3)

case.P2.f.m4 <- glmmTMB(case_daily ~ acs + com + seq.d  + acs*seq.d + com*seq.d +
                          scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                          offset(log(pop_denom)) + (seq.d|modzcta) , data = daily.p2)
saveRDS(case.P2.f.m4, "case.P2.f.m4.rds")

case.P2.f.m5 <- glmmTMB(case_daily ~ acs + com + seq.d  + acs*seq.d + com*seq.d +
                          scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                          offset(log(pop_denom)) + (seq.d|modzcta) , data = daily.p2, 
                        family = nbinom2, ziformula  = ~1)
saveRDS(case.P2.f.m5, "case.P2.f.m5.rds")

case.P2.f.m6 <- glmmTMB(case_daily ~ acs + com + seq.d + borough + acs*seq.d + com*seq.d +
                          scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                          offset(log(pop_denom)) + (seq.d|modzcta) , data = daily.p2, 
                        family = nbinom2, ziformula  = ~1)
saveRDS(case.P2.f.m6, "case.P2.f.m6.rds")


com.tab <- anova(case.P2.f.m4,case.P2.f.m5, case.P2.f.m2)


overdisp_fun <- function(model) {
  rdf <- df.residual(model)
  rp <- residuals(model,type="pearson")
  Pearson.chisq <- sum(rp^2)
  prat <- Pearson.chisq/rdf
  pval <- pchisq(Pearson.chisq, df=rdf, lower.tail=FALSE)
  c(chisq=Pearson.chisq,ratio=prat,rdf=rdf,p=pval)
}

overdisp_fun(case.P2.f.m3)
library(performance)
check_overdispersion(case.P2.f.m3)

#death 
death.P2.r.m1 <- glmmTMB(death_daily ~ acs + com + 
                            scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                            offset(log(pop_denom)) + (1|modzcta), data = daily.p2,
                          family = nbinom2, ziformula  = ~ 1)
ranef(death.P2.r.m1)
summary(death.P2.r.m1)
saveRDS(death.P2.r.m1,"death.P2.r.m1.rds")

death.P2.r.m2 <- glmmTMB(death_daily ~ acs + com + seq.d +  
                            scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                            offset(log(pop_denom)) + (1|modzcta), data = daily.p2,
                          family = nbinom2, ziformula  = ~ 1)
ranef(death.P2.r.m2)
summary(death.P2.r.m2)
saveRDS(death.P2.r.m2,"death.P2.r.m2.rds")

death.P2.r.m3 <- glmmTMB(death_daily ~ acs + com + seq.d + acs*seq.d + 
                                scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                                offset(log(pop_denom)) + (1|modzcta), data = daily.p2,
                              family = nbinom2, ziformula  = ~ 1)
summary(death.P2.r.m3)
ranef(death.P2.r.m3)
saveRDS(death.P2.r.m3, "death.P2.r.m3.rds")

death.P2.r.m4 <- glmmTMB(death_daily ~ acs + com + seq.d + com*seq.d + 
                           scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                           offset(log(pop_denom)) + (1|modzcta), data = daily.p2,
                         family = nbinom2, ziformula  = ~ 1)
summary(death.P2.r.m4)
saveRDS(death.P2.r.m4, "death.P2.r.m4.rds")

# Warning message:
# In fitTMB(TMBStruc) : Model convergence problem; false convergence (8). See vignette('troubleshooting')

death.P2.f.m1 <- glmmTMB(death_daily ~ acs + com + seq.d + acs*seq.d + com*seq.d +
                            scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                           offset(log(pop_denom)) + (1|modzcta), data = daily.p2,
                         family = nbinom2, ziformula = ~1)
summary(death.P2.f.m1)
ranef.glmmTMB(death.P2.f.m1)
saveRDS(death.P2.f.m1, "death.P2.f.m1.rds")


daily.p2$modzcta_1 <- factor(daily.p2$modzcta)
daily.p2$seq.d_1 <- factor(daily.p2$seq.d)
levels(daily.p2$seq.d_1) 

death.P2.f.m2 <- glmmTMB(death_daily ~ acs + com + seq.d + acs*seq.d + com*seq.d +
                                scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                                offset(log(pop_denom)) + (seq.d|modzcta), data = daily.p2,
                              family = nbinom2, ziformula  = ~ 1)

# death.P2.f.m2 <- glmmTMB(death_daily ~ acs + com + seq.d + acs*seq.d + com*seq.d +
                           # scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                           # offset(log(pop_denom)) + (1|modzcta) + (seq.d |modzcta), data = daily.p2,
                         # family = nbinom2, ziformula  = ~ 1)

summary(death.P2.f.m2)
ranef.glmmTMB(death.P2.f.m2)
saveRDS(death.P2.f.m2, "death.P2.f.m2.rds")

# Warning messages:
#   1: In fitTMB(TMBStruc) :
#   Model convergence problem; non-positive-definite Hessian matrix. See vignette('troubleshooting')
# 2: In fitTMB(TMBStruc) :
#   Model convergence problem; false convergence (8). See vignette('troubleshooting')


